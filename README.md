# Kubernetes | Installation de Minikube sur une instance EC2

_______


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang"> 

> **Carlin FONGANG**  | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Contexte
Ce lab vise la pise en main des étapes d'installation de l'outil minikube

## Objectifs

Dans ce lab, nous allons :

- Provisionner une instance ec2 sur la quelle nous allons par la suite effectuer toute nos opérations

- Effectuer les configurations nécessaires sur cette instance afin d'y  héberger un cluster kubernetes, en locurence **minikube**

- Déployer minikube par la suite sur notre environnement fraichement provisionné




## 1. Définition des conceptes
Minikube est un outil léger qui permet de faire fonctionner **Kubernetes** localement sur une machine avec des ressources limités. Il crée un cluster Kubernetes minimal sur une machine virtuelle ou un conteneur, idéal pour le développement et les tests. Avec Minikube, il est possible  d'expérimenter les fonctionnalités de Kubernetes sans avoir à déployer un cluster complet, facilitant l'apprentissage et l'adaptation rapide aux besoins spécifiques des projets.

## 2. **Prérequis** : Liste des exigences matérielles et logicielles.

Dans notre cas, nous allons provisionner une instances EC2 s'exécutant sous Ubuntu 20.04 Focal Fossa LTS, grace au provider AWS, à partir delaquelle nous effectuerons toutes nos opérations.

[Provisionner une instance EC2 sur AWS à l'aide de Terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws) (recommandé)

[Provisionner une instance EC2 sur AWS à l'aide d'Ansible](https://gitlab.com/CarlinFongang-Labs/Ansible/lab10-deploy-ec2)



## 4. **Choix de l'outil d'installation** : Comparaison des outils comme kubeadm, Minikube, ou Kubespray.
Lors du choix d'un outil d'installation pour Kubernetes, il est crucial de considérer vos besoins spécifiques. 

**Kubeadm** est idéal pour ceux qui souhaitent une expérience Kubernetes plus proche de la production, offrant un moyen simple d'initialiser, de configurer et de gérer des clusters (multi-node cluster).

**Minikube**, en revanche, est parfait pour les débutants ou développeurs qui ont besoin de tester Kubernetes rapidement sur leur machine locale sans configurer un cluster entier (mono-node). 

**Kubespray** se distingue par sa flexibilité, permettant le déploiement sur diverses infrastructures, ce qui en fait une option solide pour des environnements complexes, notamenet pour du Baremetal.


## 5. **Étapes d'installation détaillées**

### 5.1. Provisionnement de l'instance

1. Nous allons créer un compte AWS, en suivant les instruction via ce lien : [Créer un compte AWS](https://gitlab.com/CarlinFongang-Labs/Terraform/lab0-sigup-aws.git)

2. Une fois le compte crée, nous allons nous rendre sur la console AWS ou entrer lien console.aws.amazon.com dans le navigateur
>![alt text](img/image.png)
*Console AWS*

3. Nous allons provisionner une instance ec2 depuis la console AWS

Une fois connecté, nous allons nous rendre dans la barre de recherche de la console pour entrer EC2

>![alt text](img/image-1.png)
*Résultat des recherches*

4. Création de l'instance

Nous allons ensuite cliquer sur le boutton **Lancer une instance**
>![alt text](img/image-2.png)

Une page de configuration s'affiche 

5. Configuration de l'instance

##### Etape 1 : Nom de l'instance et choix de l'OS

Nous allons donner un nom et sélectionner l'image correspondant aux exigences citée plus haut, dans notre cas le nom de l'instance sera **minikube** et l'image de l'OS sera Ubuntu 20.04 LTS Focal, 64bits (x86)

>![alt text](img/image-3.png)

##### Etape 2 : Type d'instance et clé ssh

dans cette étape nous définissons le type d'instance à **t3.medium**, cette instance nous fournirra assez de caractéristique matérielle pour gérer de façon fuide nos tests sur minikube, nous créons également la paire de clés de sécurité qui nous permettra de nous connecté en **ssh** à l'instance ec2

>![alt text](img/image-4.png)

**Sauvegardez précieusement** cette clé ssh qui à été crée au format **.pem**, dans notre cas nous l'avons nommée **devops-aCD.pem**

##### Etape 3 : Réseau et stockage
Nous configurons dans cette étape un nouveau réseau ouvert entièrement sur internt pour des besoin de flexibilité de nos test (non recommandé), aiinsi qu'un stockage EBS minimale qui permettra de télécharger les paquets utiles sans contraintes d'espace (50 Go)

>![alt text](img/image-5.png)


Une foit ces 3 étapes réalisées, nous allons exécuter cette configuration en cliquant sur le bouton **Lancer l'instance** sur la droite de l'écran.

>![alt text](img/image-6.png)

### 5.2. Instruction de connexion à l'instance ec2

1. Une fois l'instance provisionnée, sur le volet de navigation de gauche, nous allons nous rendre sur **Instances > Instances**

>![alt text](img/image-7.png)

rendu dans la catégorie **Instances**, on peut voir sur l'écran principal l'instance **minikube** en cours d'exécution

2. Selection et option de connexion

L'on va sélectionné la **check box** positionné au début de la ligne décrivant l'instance

>![alt text](img/image-8.png)


Cette opération vas dégrissé le bouton **Se connecter** au dessus à droite de l'écran.

l'on va alors cliquer sur le bouton **Se connecter** pour avoir les option de connexion possible et se rendre dans la section **Client SSH**

>![alt text](img/image-9.png)

Des instructions pour se connecter à l'instance sont données.

### 5.3. Connexion à l'instance

Nous allons ouvrir l'invite de Commandes Windows ou le Terminal Linux, ou encore VisualStudio Code pour nous connecter à notre instance en suivant la procédure indiquée sur l'image précédente

>![alt text](img/image-10.png)

````bash
ssh -i /path/devops-aCD.pem ubuntu@ec2-3-82-4-119.compute-1.amazonaws.com
````

dans mon cas, a la suite de l'option **-i**, je vais renseigner le chemin relatif pour accéder diretement à l'emplacement où se trouve la clé ssh.

Il sufflut alors de valider la commande et de valider la prochaine étape par un **"yes"** pour se connecter 

>![alt text](img/image-11.png)
*Connexion en ssh au serveur Ubuntu*


### 5.2. Setup de l'environnement

#### 5.2.1. Mise à jour de la liste des paquets et installation des dépendances nécessaires

````bash
sudo apt-get update -y 
````

````bash
sudo apt-get install -y git libvirt-daemon-system libvirt-clients qemu-kvm virtinst bridge-utils
````

````bash
sudo apt-get install -y socat conntrack
````

#### 5.2.2. Installation de Docker

1. Téléchargement du script d'installation officiel de Docker
````bash
curl -fsSL https://get.docker.com -o get-docker.sh
````

2. Exécution du script d'installation de Docker

````bash
sudo sh get-docker.sh
````
>![alt text](img/image-12.png)


3. Ajout de l'utilisateur au groupe Docker pour pouvoir exécuter les commandes Docker sans sudo

````bash
sudo usermod -aG docker $USER
````

````bash
sudo systemctl enable --now docker
````


### 5.3. Installation de Minikube

#### 5.3.1. Téléchargement de Minikube

````bash
sudo wget https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
````

````bash
sudo chmod +x minikube-linux-amd64
````

>![alt text](img/image-13.png)
*Changement des autorisation du binaire*

````bash
sudo mv minikube-linux-amd64 /usr/local/bin/minikube
````
Cette opération permet de déplacer le binaire vers le repertoire **/usr/local/bin/**


#### 5.3.2. Installation de kubectl

Téléchargement du binaire

````bash
curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
````

Changement des autorisations
````bash
sudo chmod +x ./kubectl
````

Déplacement du binaire vers le repertoire approprié

````bash
sudo mv ./kubectl /usr/local/bin/kubectl
````


#### 5.3.3. Configuration de sysctl pour le réseau

Cette étape permet de configurer iptables pour les ponts réseau.

````bash
echo 'net.bridge.bridge-nf-call-iptables=1' | sudo tee -a /etc/sysctl.conf
````

````bash
sudo sysctl -p
````

### 5.4. Demarrage de minikube

La commande suivante démarre Minikube en utilisant Docker comme pilote. L'utilisation de **--driver=none** est spécifique à des environnements où l'on exécute tout dans un seul hôte sans virtualisation.

````bash
minikube start --driver=docker
````

>![alt text](img/image-14.png)
*Minikube est bien installé*


dans le cas où l'on ne voudrais pas que Minikube pilote un outil de virtualisation, alors on ferra :

````bash
minikube start --vm-driver=none
````

#### Configuration de l'autocomplétion

````bash
echo 'source <(kubectl completion bash)' >> ${HOME}/.bashrc && source ${HOME}/.bashrc
````

### 5.5. Vérification de l'instanllation de Minikube

````bash
minikube status
````

>![alt text](img/image-15.png)
*Minikube disponible sur notre instance*


````bash
kubectl get nodes
````

>![alt text](img/image-16.png)
*Conteneur et node en cours d'exécution*