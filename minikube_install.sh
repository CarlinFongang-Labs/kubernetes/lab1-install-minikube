#!/bin/bash
#Setup de l'environnement
sudo apt-get update -y 
sudo apt-get install -y git libvirt-daemon-system libvirt-clients qemu-kvm virtinst bridge-utils
sudo apt-get install -y socat conntrack
sleep 5

#Installation de Docker
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo usermod -aG docker $USER
sudo systemctl enable --now docker

#Installation de Minikube
sudo wget https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo chmod +x minikube-linux-amd64
sudo mv minikube-linux-amd64 /usr/local/bin/minikube

#Installation de kubectl
curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
sudo chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl

#Configuration de sysctl pour le réseau
echo 'net.bridge.bridge-nf-call-iptables=1' | sudo tee -a /etc/sysctl.conf
sudo sysctl -p

#Demarrage de minikube
minikube start --driver=docker #minikube start --vm-driver=none
sleep 5
ls
#Configuration de l'autocomplétion
echo 'source <(kubectl completion bash)' >> ${HOME}/.bashrc && source ${HOME}/.bashrc